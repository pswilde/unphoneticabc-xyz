---
title: "Weisbeir"
date: 2021-04-02T20:52:02+01:00
draft: false
---
a beer, traditionally from Bavaria, in which a significant proportion of malted barley is replaced with malted wheat
