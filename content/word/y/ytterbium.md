---
title: "Ytterbium"
date: 2021-04-02T20:52:40+01:00
draft: false
---
a soft, malleable and ductile chemical element that displays a bright silvery luster when pure.
