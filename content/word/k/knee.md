---
title: "Knee"
date: 2021-04-02T20:44:33+01:00
draft: false
---
a joint in the middle part of the human leg that is the articulation between the femur, tibia, and patella
