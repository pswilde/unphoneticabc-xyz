---
title: "Phishing"
date: 2021-04-02T20:49:51+01:00
draft: false
---
a cybercrime in which a target or targets are contacted by email, telephone or text message by someone posing as a legitimate institution
