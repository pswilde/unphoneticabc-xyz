---
title: "Xylophone"
date: 2021-04-02T20:52:19+01:00
draft: false
---
a musical instrument in the percussion family that consists of wooden bars struck by mallets.
