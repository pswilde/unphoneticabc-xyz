---
title: "Euphemism"
date: 2021-04-02T20:40:50+01:00
draft: false
---
the substitution of an agreeable or inoffensive expression for one that may offend or suggest something unpleasant 
